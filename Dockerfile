FROM python:3.8
WORKDIR /predict_activity
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 8000
COPY . .
ADD start.sh /
RUN chmod +x /start.sh
CMD ["/start.sh"]
