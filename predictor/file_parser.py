import pandas as pd

from .ml_model_wrapper import get_new_model, save_model, SETTINGS
from .sql_alchemy_wrapper import get_connection


def parse_files_to_db(file_data, file_history_data):
    file_data_df = pd.read_csv(file_data)
    file_history_data_df = pd.read_csv(file_history_data)
    file_data_df['day'] = pd.to_datetime(file_data_df['day'], format="%Y-%m-%d")
    file_history_data_df['day'] = pd.to_datetime(file_history_data_df['Date time'], format="%m/%d/%Y")
    file_history_data_df.drop(['Date time'], axis=1, inplace=True)
    file_history_data_df.fillna(value=0, inplace=True)
    file_history_data_df.drop(
        [
            'Resolved Address',
            'Location',
            'Address',
            'Conditions',
        ],
        axis=1,
        inplace=True
    )

    joined_data = pd.merge(file_data_df, file_history_data_df, on='day')
    joined_data.drop(joined_data.columns[0], axis=1, inplace=True)
    joined_data.index = range(1, len(joined_data) + 1)

    # joined_data.drop(['keywordId', 'adgroupId'], axis=1, inplace=True)
    # joined_data = joined_data.groupby(['accountId', 'campaignId']).mean()

    connection = get_connection()

    drop_statement = "DROP TABLE account_data"
    connection.execute(drop_statement)

    joined_data[['accountId', 'campaignId', 'adgroupId', 'keywordId']].to_sql('account_data', con=connection)

    # Preparing ML models
    train_data = joined_data.drop(['clicks', 'impressions', 'day', 'Heat Index', 'Snow Depth'], axis=1)
    train_data = train_data[SETTINGS['model_fields']]

    clicks_labels = joined_data['clicks']
    impressions_labels = joined_data['impressions']

    # creating model for predicting clicks
    clicks_model = get_new_model()
    clicks_model.fit(train_data, clicks_labels)
    save_model(clicks_model, 'clicks_model.sav')

    # creating model for predicting impressions
    impressions_model = get_new_model()
    impressions_model.fit(train_data, impressions_labels)
    save_model(impressions_model, 'impressions_model.sav')
