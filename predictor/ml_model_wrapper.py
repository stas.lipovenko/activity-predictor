import pickle

from sklearn.linear_model import LinearRegression

MODEL = LinearRegression
SETTINGS = {
    'model_fields': ['accountId', 'adgroupId', 'campaignId', 'keywordId',
                     'Maximum Temperature', 'Minimum Temperature', 'Temperature',
                     'Wind Chill', 'Precipitation', 'Wind Speed', 'Wind Gust', 'Cloud Cover',
                     'Relative Humidity']
}


def get_new_model():
    return MODEL()


def save_model(model, filename: str):
    try:
        pickle.dump(model, open(filename, 'wb'))
        return True
    except Exception:
        return False


def load_model(filename: str):
    try:
        model = pickle.load(open(filename, 'rb'))
        return model
    except Exception:
        return None
