# Create your views here.

import pandas as pd
from django.http import HttpResponse
from django.shortcuts import render
from django.views import View

from .file_parser import parse_files_to_db
from .forecast_api_caller import get_weather_data_dict, LOCATION
from .forms import UploadCsvFilesForm
from .ml_model_wrapper import load_model, SETTINGS
from .sql_alchemy_wrapper import get_connection


class CSVParser(View):
    def post(self, request, *args, **kwargs):
        try:
            form = UploadCsvFilesForm(request.POST, request.FILES)
            if form.is_valid():
                parse_files_to_db(request.FILES['file_data'], request.FILES['file_history_data'])
                return render(request, 'predictor/upload_and_parse_form.html', context={'form': form, 'uploaded': True})
        except Exception:
            return HttpResponse(status=500)

    def get(self, request, *args, **kwargs):
        form = UploadCsvFilesForm()
        return render(request, 'predictor/upload_and_parse_form.html', context={'form': form, 'uploaded': False})


class PredictImpressionClicks(View):
    def get(self, request, account_id, date):
        try:
            forecast = get_weather_data_dict(date)['locations'][LOCATION]['values'][0]

            connection = get_connection()

            pd.read_sql('account_data', con=connection)
            account_data = pd.read_sql('account_data', con=connection, index_col='index')
            account_data = account_data.loc[account_data['accountId'] == account_id]

            account_data['Maximum Temperature'] = forecast['maxt']
            account_data['Minimum Temperature'] = forecast['mint']
            account_data['Temperature'] = forecast['temp']
            account_data['Wind Chill'] = forecast['windchill']
            account_data['Precipitation'] = forecast['precip']
            account_data['Wind Speed'] = forecast['wspd']
            account_data['Wind Gust'] = forecast['wgust']
            account_data['Cloud Cover'] = forecast['cloudcover']
            account_data['Relative Humidity'] = forecast['humidity']

            account_data.fillna(value=0, inplace=True)
            predict_data = account_data[SETTINGS['model_fields']]

            clicks_model = load_model('clicks_model.sav')
            clicks_result = clicks_model.predict(predict_data)

            impressions_model = load_model('impressions_model.sav')
            impressions_result = impressions_model.predict(predict_data)

            account_data['clicks'] = clicks_result
            account_data['impressions'] = impressions_result

            account_data = account_data[['accountId', 'adgroupId', 'campaignId', 'keywordId', 'clicks', 'impressions']]
            result = account_data.groupby(by=['accountId', 'campaignId', 'adgroupId', 'keywordId']).mean()

            return HttpResponse(result.to_json(orient='table'))
        except Exception as e:
            print(e)
            return HttpResponse(status=500)
