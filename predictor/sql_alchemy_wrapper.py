from sqlalchemy import create_engine


def get_connection():
    return create_engine('sqlite:///db.sqlite3')
