import requests
from datetime import datetime
from json import loads

VISUAL_CROSSING_KEY = 'FCV3XH2585XK00I8NM4GDGCPP'
LOCATION = 'Stockholm'


def get_weather_data_dict(date):
    datetime_str = datetime.strptime(date, '%Y-%m-%d').strftime('%Y-%m-%dT00:00:00')
    url = 'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/weatherdata/history'
    params = {
        'aggregateHours': 24,
        'startDateTime': datetime_str,
        'endDateTime': datetime_str,
        'unitGroup': 'uk',
        'contentType': 'json',
        'dayStartTime': '0:0:00',
        'dayEndTime': '0:0:00',
        'locations': LOCATION,
        'key': VISUAL_CROSSING_KEY
    }

    forecast = requests.get(url, params=params)
    result = loads(forecast.content)
    return result
