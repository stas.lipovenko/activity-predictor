from django import forms


class UploadCsvFilesForm(forms.Form):
    file_data = forms.FileField()
    file_history_data = forms.FileField()
