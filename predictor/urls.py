from django.urls import path

from . import views

urlpatterns = [
    path('upload_and_parse/', views.CSVParser.as_view(), name='upload_and_parse'),
    path('predict_impressions_clicks/<int:account_id>/<str:date>', views.PredictImpressionClicks.as_view(),
         name='predict')
]
