from django.apps import AppConfig


class CsvParserConfig(AppConfig):
    name = 'predictor'
